#!/bin/bash -x
### Install jitsi-meet:
### https://github.com/jitsi/jitsi-meet/blob/master/doc/quick-install.md

set -e   # stop on error

source /host/settings.sh

main() {
    install-jitsi-meet
    update-videobridge-config
    [[ -n $MEET_USER ]] && secure-domain
}

install-jitsi-meet() {
    echo "jitsi-videobridge jitsi-videobridge/jvb-hostname string $DOMAIN" | debconf-set-selections
    echo "jitsi-meet jitsi-meet/cert-choice select Self-signed certificate will be generated" | debconf-set-selections
    DEBIAN_FRONTEND=noninteractive \
                   apt install --yes jitsi-meet
}

update-videobridge-config() {
    # create a script that updates the videobridge config
    # with the local and public IP addresses (needed for NAT)
    # see:
    # https://github.com/jitsi/jitsi-meet/blob/master/doc/quick-install.md#advanced-configuration
    cat << EOF > /usr/local/sbin/update-videobridge-config.sh
#!/bin/bash

local_address=\$(hostname -I | xargs)
public_address=\$(wget -qO- https://ipecho.net/plain)

sed -i /etc/jitsi/videobridge/sip-communicator.properties \\
    -e '/NAT_HARVESTER_LOCAL_ADDRESS/,+1 d'

echo "org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS=\$local_address" \\
    >> /etc/jitsi/videobridge/sip-communicator.properties
echo "org.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=\$public_address" \\
    >> /etc/jitsi/videobridge/sip-communicator.properties

systemctl restart jitsi-videobridge2
EOF
    chmod +x /usr/local/sbin/update-videobridge-config.sh

    # make a systemd service that runs the script above on boot
    cat << EOF > /etc/systemd/system/update-videobridge-config.service
[Unit]
RequiredBy=jitsi-videobridge2.service

[Service]
ExecStart=/usr/local/sbin/update-videobridge-config.sh

[Install]
WantedBy=default.target
EOF

    # enable and start this service
    systemctl daemon-reload
    systemctl enable update-videobridge-config.service
    systemctl start update-videobridge-config.service
}

secure-domain() {
    # ask for a password when creating a new room
    # https://github.com/jitsi/jicofo#secure-domain

    # on prosody, enable authentication on the main domain
    local config_file=/etc/prosody/conf.avail/$DOMAIN.cfg.lua
    sed -i $config_file \
        -e "/VirtualHost \"$DOMAIN\"/,+3 s/authentication = .*/authentication = \"internal_plain\"/"

    # on prosody, add new virtual host with anonymous login method for guests
    sed -i $config_file \
        -e "/VirtualHost \"guest.$DOMAIN\"/,+2 d"
    cat <<EOF >> $config_file
VirtualHost "guest.$DOMAIN"
    authentication = "anonymous"
    c2s_require_encryption = false
EOF

    # in jitsi-meet config.js uncomment and set anonymousdomain
    sed -i /etc/jitsi/meet/$DOMAIN-config.js \
        -e "s#// anonymousdomain: .*#anonymousdomain: 'guest.$DOMAIN',#"

    # setup jicofo to accept conference allocation requests only from authenticated domain
    echo "org.jitsi.jicofo.auth.URL=XMPP:$DOMAIN" >> /etc/jitsi/jicofo/sip-communicator.properties
}

# call the main function
main
