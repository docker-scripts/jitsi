include(focal)

### update and upgrade
RUN apt update &&\
    apt upgrade --yes

### unminimize
RUN DEBIAN_FRONTEND=noninteractive \
    yes | unminimize

RUN apt install --yes wget gnupg apt-transport-https apache2
#RUN apt install --yes wget gnupg apt-transport-https nginx

RUN echo 'deb https://download.jitsi.org stable/' > /etc/apt/sources.list.d/jitsi-stable.list &&\
    wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | apt-key add - &&\
    apt update

