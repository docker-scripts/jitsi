cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    check_settings

    ds inject ubuntu-fixes.sh
    ds inject ssmtp.sh
    ds inject logwatch.sh $(hostname)

    ds inject jitsi-meet.sh
    [[ -z $MEET_USER ]] || \
        ds register $MEET_USER $MEET_PASS
}

check_settings() {
    [[ $MEET_USER == 'host' ]] && [[ $MEET_PASS == 'pass123' ]] &&\
	    echo "Error: For security reasons please change MEET_USER and/or MEET_PASS on 'settings.sh'." >&2 &&\
        exit 1
}
