cmd_register_help() {
    cat <<_EOF
    register <username> <password>
        Register a user that can create meeting rooms.

_EOF
}

cmd_register() {
    local username=$1
    local password=$2
    [[ -z $password ]] && echo -e "Usage: \n$(cmd_register_help)\n" && exit 1
    
    ds shell \
       prosodyctl register $username $DOMAIN $password
}
