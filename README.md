# jitsi-meet container

Jitsi Meet is a fully encrypted, 100% open source video conferencing
solution that you can use all day, every day, for free — with no
account needed: https://jitsi.org/jitsi-meet/

## Installation

  - First install `ds` and `revproxy`:
      + https://gitlab.com/docker-scripts/ds#installation
      + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull jitsi`

  - Create a directory for the container: `ds init jitsi @meet.example.org`

  - Fix the settings: `cd /var/ds/meet.example.org/ ; vim settings.sh`

  - Build image, create the container and configure it: `ds make`

## Installing on a LAN

If the server where you are installing Jitsi Meet is on a LAN, edit
`settings.sh` and set `DOMAIN=<hostname>.local`, where '<hostname>' is
the name of the local host (as reported by the command `hostname`).
Then the other clients on the LAN can access the jitsi server on
`https://<hostname>.local/`.

This works because of mDNS, and needs Avahi to work (in ubuntu/debian
it is the package `avahi-daemon`).

**Note:** Android does not support yet mDNS, so you will not be able
to access such a local server from Android devices.

**Note:** Why would someone use jitsi-meet on a LAN?  Because of chat
and screen sharing. It may be useful on a classroom to send URL-s to
the students, or to share your screen with them. Showing your screen
to the students is very useful for example when teaching programming
(especially when there is no projector).

## Other commands

```
ds register <user> <pass>
ds stop
ds start
ds shell
ds help
```
