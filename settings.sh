APP=jitsi
PORTS="4443:4443 10000:10000"

### If you are installing on a LAN, use the domain 'hostname.local'
### instead, where 'hostname' is the name of the local host (as
### reported by the command `hostname`). In debian/ubuntu this needs
### the package 'avahi-daemon' to work.
DOMAIN=meet.example.org
#DOMAIN=hostname.local

### User that can create new meeting rooms.
### Other users can be registered later with
### `ds register <user> <pass>`
### If this is commented, then everyone
### will be allowed to create new meeting rooms.
MEET_USER=host
MEET_PASS=pass123
